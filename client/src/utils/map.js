import store from "../store/index";
import { f } from "./fetch";

export let Mymap = {
  // initialisation de la map
  lat: 45.782,
  lng: 4.8656,
  zoom: 19,
  mymap: {},
  L: {},
  nautibusIcon: {},
  markerPlayer: {},
  markerZRR: {},
  playerIcon: {},
  treasureMarker: [],
  treasureIcon: {},
  markerJoueurs: [],
  alive: true,

  async createMap() {
    // HERE is where to load Leaflet components!
    this.L = await import("leaflet");

    // Procédure d'initialisation
    this.mymap = this.L.map("map", {
      center: [this.lat, this.lng],
      zoom: this.zoom,
    });

    // Création d'un "tile layer" (permet l'affichage sur la carte)
    this.L.tileLayer(
      "https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.jpg90?access_token=pk.eyJ1IjoieGFkZXMxMDExNCIsImEiOiJja2d3NjJ4c28wNzQ1MnlyMTM1cjEwb2NxIn0.DvtwpmO4QPeZNY6h1rqRVw",
      {
        maxZoom: 22,
        minZoom: 1,
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
          '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
          'Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
        id: "mapbox/streets-v11",
        tileSize: 512,
        zoomOffset: -1,
        accessToken:
          "pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA",
      }
    ).addTo(this.mymap);

    // Ajout de l'icon du joueur
    this.playerIcon = this.L.icon({
      iconUrl: require("../assets/player.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    this.playerOtherIcon = this.L.icon({
      iconUrl: require("../assets/playerOther.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    this.playerDeadIcon = this.L.icon({
      iconUrl: require("../assets/playerDead.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    this.playerInvisibleIcon = this.L.icon({
      iconUrl: require("../assets/playerInvisible.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    this.treasureIcon = this.L.icon({
      iconUrl: require("../assets/treasure.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    this.treasureIconOpen = this.L.icon({
      iconUrl: require("../assets/treasureOpen.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    this.nautibusIcon = this.L.icon({
      iconUrl: require("../assets/logo.png"),
      iconSize: [38, 38],
      //iconAnchor: [22, 94],
      //popupAnchor: [-3, -76],
    });

    // Ajout du Marker Entree de Nautibus
    this.L.marker([45.78207, 4.86559], { icon: this.nautibusIcon })
      .addTo(this.mymap)
      .bindPopup("Entrée du bâtiment<br><strong>Nautibus</strong>.")
      .openPopup();

    // Recup la position actuelle et envoie au serveur
    navigator.geolocation.watchPosition(
      (position) => {
        const { latitude, longitude } = position.coords;

        this.lat = latitude;
        this.lng = longitude;

        if (store.state.start) {
          if (store.state.joueur.ttl != 0) {
            store.state.joueur.position = { lat: this.lat, lng: this.lng };
            this.changeIconPlayer();
            store.state.joueur.distance_coffre =
              this.distancePlayerTreasure().toFixed(1);
          }
          f.updateUserPosition({ lat: this.lat, lng: this.lng });
        }
      },
      (error) => {
        const { code } = error;
        switch (code) {
          case GeolocationPositionError.TIMEOUT:
            store.state.error =
              "TIMEOUT: nous n'avons pas pu obtenir votre position.";
            break;
          case GeolocationPositionError.PERMISSION_DENIED:
            store.state.error =
              "ERREUR: nous avons besoin de votre permission pour acceder a votre position.";
            break;
          case GeolocationPositionError.POSITION_UNAVAILABLE:
            store.state.error = "ERREUR: nous ne trouvons pas votre position";
            break;
        }
      },
      { timeout: 60_000 }
    );

    // Clic sur la carte mock
    this.mymap.on("click", (e) => {
      this.lat = e.latlng.lat;
      this.lng = e.latlng.lng;
      if (store.state.start) {
        if (store.state.joueur.ttl != 0) {
          store.state.joueur.position = { lat: this.lat, lng: this.lng };
          this.changeIconPlayer();
          store.state.joueur.distance_coffre =
            this.distancePlayerTreasure().toFixed(1);
          f.updateUserPosition();
        }
      }
    });
  },

  createZrr() {
    if (this.markerZRR != null) {
      this.mymap.removeLayer(this.markerZRR);
    }
    if (
      store.state.ZRR._southWest != undefined &&
      store.state.ZRR._northEast != undefined
    ) {
      this.markerZRR = this.L.rectangle(
        [store.state.ZRR._southWest, store.state.ZRR._northEast],
        {
          color: "#ff7800",
          weight: 1,
        }
      ).addTo(this.mymap);
    }
  },

  printTreasure() {
    this.treasureMarker.forEach((element) => {
      this.mymap.removeLayer(element);
    });
    this.treasureMarker = [];
    store.state.coffres.forEach((element) => {
      store.state.start = true;
      this.treasureMarker.push(
        this.L.marker([element.positions.lat, element.positions.lng], {
          icon: element.trouver ? this.treasureIconOpen : this.treasureIcon,
        })
          .addTo(this.mymap)
          .bindPopup("treasure" + element.id)
      );
    });
  },

  printJoueur() {
    let markerPoint = undefined;
    this.markerJoueurs.forEach((element) => {
      this.mymap.removeLayer(element);
    });
    this.markerJoueurs = [];
    store.state.joueurs.forEach((joueur) => {
      if (joueur.id != store.state.login) {
        markerPoint = this.L.marker(
          [joueur.position.lat, joueur.position.lng],
          {
            icon:
              joueur.ttl > 0 || joueur.ttl == -1
                ? this.playerOtherIcon
                : this.playerDeadIcon,
          }
        );
        markerPoint
          .addTo(this.mymap)
          .bindPopup("<strong>" + joueur.id + "</strong>.");
        this.markerJoueurs.push(markerPoint);
      } else {
        this.changeIconPlayer();
      }
    });
  },

  changeIconPlayer() {
    if (this.markerPlayer != null) {
      let iconPlayer = undefined;
      if (store.state.joueur == {}) {
        return;
      } else if (store.state.joueur.ttl === 0) {
        iconPlayer = this.playerDeadIcon;

        if (this.alive) {
          this.alive = false;
          window.navigator.vibrate(1000);
          Notification.requestPermission().then((result) => {
            if (result === "granted") {
              new Notification("Vous êtes mort!", {
                body: "Votre temps est écoulé ou vous avez ramassé un coffre piègé !",
                icon: this.playerDeadIcon,
              });
            }
          });
        }
      } else if (store.state.joueur.ttl == -1) {
        iconPlayer = this.playerInvisibleIcon;
      } else {
        iconPlayer = this.playerIcon;
        this.alive = true;
      }

      this.mymap.removeLayer(this.markerPlayer);
      this.markerPlayer = this.L.marker(
        [store.state.joueur.position.lat, store.state.joueur.position.lng],
        {
          icon: iconPlayer,
        }
      );
      this.markerPlayer
        .addTo(this.mymap)
        .bindPopup("<strong>" + "VOUS" + "</strong>.");
    }
  },

  degrees_to_radians(degrees) {
    let pi = Math.PI;
    return degrees * (pi / 180);
  },

  distanceBewteenTwoPoint(lat1, lat2, lng1, lng2) {
    return (
      Math.acos(
        Math.sin(this.degrees_to_radians(lat1)) *
          Math.sin(this.degrees_to_radians(lat2)) +
          Math.cos(this.degrees_to_radians(lat1)) *
            Math.cos(this.degrees_to_radians(lat2)) *
            Math.cos(this.degrees_to_radians(lng2 - lng1))
      ) *
      6371 *
      1000
    );
  },

  distancePlayerTreasure: function () {
    let minDist = -1;
    store.state.coffres.forEach((element) => {
      let distance = this.distanceBewteenTwoPoint(
        element.positions.lat,
        store.state.joueur.position.lat,
        element.positions.lng,
        store.state.joueur.position.lng
      );
      if (minDist == -1 || minDist > distance) {
        minDist = distance;
      }
    });
    return minDist;
  },
};
