import store from "../store/index";
import { Mymap } from "./map";

export let f = {
  state: {
    casUrl: "https://192.168.75.17:8443/cas/",

    apiUrl: "https://192.168.75.17/game/api/",

    headers() {
      const headers = new Headers();
      headers.set("Accept", "application/json");
      headers.set("Content-Type", "application/json");
      if (sessionStorage.getItem("token")) {
        headers.set("Authorization", sessionStorage.getItem("token"));
      }
      return headers;
    },
  },

  filterHttpResponse(response) {
    if (response.status >= 400 && response.status < 600) {
      throw new Error(`${response.status}: ${response.statusText}`);
    } else {
      return response;
    }
  },

  filterHttpResponseJson(response) {
    return response.json().then((data) => {
      if (response.status >= 400 && response.status < 600) {
        throw new Error(`${data.name}: ${data.message}`);
      }
      return data;
    });
  },

  createUser(login, password) {
    // signin
    return fetch(this.state.casUrl + "users", {
      method: "POST",
      headers: this.state.headers(),
      body: `{
        "login": "${login}",
        "password": "${password}"
      }`,
    }).then(this.filterHttpResponse);
  },

  login(login, password) {
    // login
    const params = {
      login: login,
      password: password,
    };
    const query = Object.keys(params)
      .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");
    return fetch(this.state.casUrl + "login?" + query, {
      method: "POST",
      headers: this.state.headers(),
    })
      .then(this.filterHttpResponse)
      .then((response) => {
        sessionStorage.setItem("token", response.headers.get("Authorization"));
        store.dispatch("SET_LOGIN", login);
      });
  },

  getRessources() {
    if (sessionStorage.getItem("token") != "") {
      fetch(this.state.apiUrl + "ressources", {
        method: "GET",
        headers: this.state.headers(),
      })
        .then(this.filterHttpResponseJson)
        .then((res) => {
          store.dispatch("SET_ZRR", res.ZRR);
          store.dispatch("SET_JOUEURS", res.users);
          let reset = true;
          res.users.forEach((element) => {
            if (element.id == store.state.login) {
              reset = false;
              store.dispatch("SET_JOUEUR", element);
              store.state.joueur.distance_coffre = element.distanceTreasure;
              Mymap.changeIconPlayer();
            }
          });
          if (reset) {
            store.dispatch("SET_JOUEUR", {});
          }
          store.dispatch("SET_COFFRES", res.treasures);
          Mymap.createZrr();
          Mymap.printTreasure();
          Mymap.printJoueur();
        });
    }
  },
  updateUserPosition(position) {
    fetch(this.state.apiUrl + "ressources/" + store.state.login, {
      method: "PUT",
      headers: this.state.headers(),
      body: JSON.stringify(position),
    }).then(this.filterHttpResponse);
  },
};

//CYCLES

function decreaseTTL() {
  if (store.state.start) {
    if (store.state.joueur != {}) {
      if (store.state.joueur.ttl > 0) {
        store.state.joueur.ttl = store.state.joueur.ttl - 1;
      }
    }
  }
}

setInterval(decreaseTTL, 1000);
