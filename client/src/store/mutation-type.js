// mutation-type

export const SET_ZRR = "SET_ZRR";
export const SET_LOGIN = "SET_LOGIN";
export const SET_JOUEUR = "SET_JOUEUR";
export const SET_JOUEURS = "SET_JOUEURS";
export const SET_POSITION_JOUEUR = "SET_POSITION_JOUEUR";
export const SET_TTL_JOUEUR = "SET_TTL_JOUEUR";
export const SET_ICONE_JOUEUR = "SET_ICONE_JOUEUR";
export const SET_COFFRES = "SET_COFFRES";
export const SET_COFFRE_TROUVE = "SET_COFFRE_TROUVE";
export const SET_DISTANCE_COFFRE_JOUEUR = "SET_DISTANCE_COFFRE_JOUEUR";
export const SET_ERROR = "SET_ERROR";
