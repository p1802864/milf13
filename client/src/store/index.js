import { createStore } from "vuex";

// mutation-type
import {
  SET_ZRR,
  SET_LOGIN,
  SET_JOUEUR,
  SET_JOUEURS,
  SET_POSITION_JOUEUR,
  SET_TTL_JOUEUR,
  SET_ICONE_JOUEUR,
  SET_COFFRES,
  SET_COFFRE_TROUVE,
  SET_DISTANCE_COFFRE_JOUEUR,
  SET_ERROR,
} from "./mutation-type.js";

const store = createStore({
  state: {
    ZRR: {},
    login: "",
    joueur: {},
    joueurs: [],
    coffres: [],
    start: false,
    error: "",
  },
  getters: {
    getZRR(state) {
      return state.ZRR;
    },
    getPositionJoueur(state) {
      return state.joueur.position;
    },
    getJoueurs(state) {
      return state.joueurs;
    },
  },
  mutations: {
    [SET_ZRR](state, coordonneZrr) {
      state.ZRR = coordonneZrr;
    },
    [SET_LOGIN](state, _login) {
      state.login = _login;
    },
    [SET_JOUEUR](state, user) {
      state.joueur = user;
    },
    [SET_JOUEURS](state, users) {
      state.joueurs = users;
    },
    [SET_POSITION_JOUEUR](state, id, coordonneJoueur) {
      state.joueurs = state.joueurs.map((u) => {
        if (u.id == id) {
          u.position = coordonneJoueur;
        }
      });
    },
    [SET_TTL_JOUEUR](state, id, _ttl) {
      state.joueurs = state.joueurs.map((u) => {
        if (u.id == id) {
          u.ttl = _ttl;
        }
      });
    },
    [SET_ICONE_JOUEUR](state, id, _icone) {
      state.joueurs = state.joueurs.map((u) => {
        if (u.id == id) {
          u.url = _icone;
        }
      });
    },
    [SET_COFFRES](state, _coffres) {
      state.coffres = _coffres;
    },
    [SET_COFFRE_TROUVE](state, _id) {
      state.coffres.map((coffre) => {
        if (coffre.id == _id) {
          coffre.trouve = true;
        }
      });
    },
    [SET_DISTANCE_COFFRE_JOUEUR](state, id, _distance) {
      state.joueurs = state.joueurs.map((u) => {
        if (u.id == id) {
          u.distanceTreasure = _distance;
        }
      });
    },
    [SET_ERROR](state, e) {
      state.error = e;
    },
  },
  actions: {
    [SET_ZRR]({ commit }, coordonneZrr) {
      commit("SET_ZRR", coordonneZrr);
    },
    [SET_LOGIN]({ commit }, _login) {
      commit("SET_LOGIN", _login);
    },
    [SET_JOUEUR]({ commit }, _user) {
      commit("SET_JOUEUR", _user);
    },
    [SET_JOUEURS]({ commit }, _user) {
      commit("SET_JOUEURS", _user);
    },
    [SET_POSITION_JOUEUR]({ commit }, id, coordonneJoueur) {
      commit("SET_POSITION_JOUEUR", id, coordonneJoueur);
    },
    [SET_TTL_JOUEUR]({ commit }, id, _ttl) {
      commit("SET_TTL_JOUEUR", id, _ttl);
    },
    [SET_ICONE_JOUEUR]({ commit }, id, _icone) {
      commit("SET_ICONE_JOUEUR", id, _icone);
    },
    [SET_COFFRES]({ commit }, _coffres) {
      commit("SET_COFFRES", _coffres);
    },
    [SET_COFFRE_TROUVE]({ commit }, _id) {
      commit("SET_COFFRE_TROUVE", _id);
    },
    [SET_DISTANCE_COFFRE_JOUEUR]({ commit }, id, _distance) {
      commit("SET_DISTANCE_COFFRE_JOUEUR", id, _distance);
    },
    [SET_ERROR]({ commit }, e) {
      commit("SET_ERROR", e);
    },
  },
  modules: {},
});

export default store;
