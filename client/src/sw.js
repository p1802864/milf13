/* eslint-disable no-undef */

if (workbox) {
  workbox.core.skipWaiting();
  workbox.core.clientsClaim();
  workbox.precaching.cleanupOutdatedCaches();

  workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);

  //Only if you use google analytics and wants to send the offline views
  workbox.googleAnalytics.initialize();

  //This is how you can use the network first strategy for files ending with .js
  workbox.routing.registerRoute(/.*\.js/, workbox.strategies.networkFirst());

  // Use cache but update cache files in the background ASAP
  workbox.routing.registerRoute(
    /.*\.css/,
    workbox.strategies.staleWhileRevalidate({
      cacheName: "css-cache",
    })
  );

  //Cache first, but defining duration and maximum files
  workbox.routing.registerRoute(
    /.*\.(?:png|jpg|jpeg|svg|gif)/,
    workbox.strategies.cacheFirst({
      cacheName: "image-cache",
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 20,
          maxAgeSeconds: 7 * 24 * 60 * 60,
        }),
      ],
    })
  );

  workbox.routing.registerRoute(
    new RegExp("https://fonts.(?:googleapis|gstatic).com/(.*)"),
    workbox.strategies.cacheFirst({
      cacheName: "googleapis",
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 30,
        }),
      ],
    })
  );
}

self.addEventListener("push", function (event) {
  const payload = event.data ? event.data.text() : "no payload";
  event.waitUntil(
    self.registration.showNotification("ServiceWorker Cookbook", {
      body: payload,
    })
  );
});
