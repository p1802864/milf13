const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    port: 3376,
  },
  configureWebpack: {},
  pwa: {
    name: "La Guilde des Trouvailles",
    themeColor: "#33261B",
    msTileColor: "#000000",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",

    workboxPluginMode: 'InjectManifest',
    workboxOptions: {

      swSrc: './src/sw.js'
      
    }
  },
});
