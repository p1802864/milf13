///////////////////////////////////////////////////////////////////////
// VARIABLES GLOABALS
///////////////////////////////////////////////////////////////////////

// initialisation de la map
let lat = 45.782, lng = 4.8656, zoom = 19;

// Ajout de l'icon du joueur
let playerIcon = this.L.icon({
	iconUrl: "assets/player.png",
	iconSize: [38, 38],
	//iconAnchor: [22, 94],
	//popupAnchor: [-3, -76],
});

let playerDeadIcon = this.L.icon({
	iconUrl: "assets/playerDead.png",
	iconSize: [38, 38],
	//iconAnchor: [22, 94],
	//popupAnchor: [-3, -76],
});

let playerInvisibleIcon = this.L.icon({
	iconUrl: "assets/playerInvisible.png",
	iconSize: [38, 38],
	//iconAnchor: [22, 94],
	//popupAnchor: [-3, -76],
});

///////////////////////////////////////////////////////////////////////
// MAPS
///////////////////////////////////////////////////////////////////////

let mymap = L.map('map', {
	center: [lat, lng],
	zoom: zoom
});
updateMap();

// Création d'un "tile layer" (permet l'affichage sur la carte)
L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.jpg90?access_token=pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA', {
	maxZoom: 22,
	minZoom: 1,
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
		'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	id: 'mapbox/streets-v11',
	tileSize: 512,
	zoomOffset: -1,
	accessToken: 'pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA'
}).addTo(mymap);

// Ajout d'un marker
L.marker([45.78207, 4.86559]).addTo(mymap).bindPopup('Entrée du bâtiment<br><strong>Nautibus</strong>.').openPopup();

// Clic sur la carte
mymap.on('click', e => {
	lat = e.latlng.lat;
	lng = e.latlng.lng;

	if (TREASURES.fireTriggered && ZRR.rectangleZRR != null) {
		if (ZRR.rectangleZRR._bounds._southWest.lat < lat &&
			ZRR.rectangleZRR._bounds._southWest.lng < lng &&
			ZRR.rectangleZRR._bounds._northEast.lat > lat &&
			ZRR.rectangleZRR._bounds._northEast.lng > lng) {

			let treasure = createTreasure();
			TREASURES.treasuresLocal.push(treasure);
		}
	}

	else if (ZRR.setMarker1) { // Place le premier point de la ZRR
		if (ZRR.markerPoint1 != null) {
			mymap.removeLayer(ZRR.markerPoint1);
		}
		ZRR.markerPoint1 = createPointZRR(ZRR.markerPoint1, "Premier Point", lat, lng);
		updateZRR();
		ZRR.setMarker1 = false;
	}

	else if (ZRR.setMarker2) { // Place le second point de la ZRR
		if (ZRR.markerPoint2 != null) {
			mymap.removeLayer(ZRR.markerPoint2);
		}
		ZRR.markerPoint2 = createPointZRR(ZRR.markerPoint2, "Second Point", lat, lng);
		updateZRR();
		ZRR.setMarker2 = false;
	}
});

// Mise à jour de la map
function updateMap() {
	// Affichage à la nouvelle position
	//mymap.setView([lat, lng], zoom);

	// La fonction de validation du formulaire renvoie false pour bloquer le rechargement de la page.
	return false;
}
