///////////////////////////////////////////////////////////////////////
// VARIABLES GLOABALS
///////////////////////////////////////////////////////////////////////

// ZRR
const ZRR = {
	markerPoint1: null, // Premier point pour définir la zone de la ZRR
	markerPoint2: null, // Second point pour définir la zone de la ZRR
	setMarker1: false, // Permet de placer le point 1 de la ZRR sur la carte
	setMarker2: false, // Permet de placer le point 2 de la ZRR sur la carte
	rectangleZRR: null // La zone de la ZRR
};


///////////////////////////////////////////////////////////////////////
// ZRR Functions
///////////////////////////////////////////////////////////////////////

/**
 * Update corners of the ZRR
 */
function updatePointsZRR(markerPoint) {
	if (markerPoint._popup._content == '<strong>Premier Point</strong>.') {
		document.getElementById("lat1").value = markerPoint.getLatLng().lat;
		document.getElementById("lon1").value = markerPoint.getLatLng().lng;
	}
	else if (markerPoint._popup._content == '<strong>Second Point</strong>.') {
		document.getElementById("lat2").value = markerPoint.getLatLng().lat;
		document.getElementById("lon2").value = markerPoint.getLatLng().lng;
	}
}

/**
 * Create the ZRR
 */
function updateZRR() {
	if (ZRR.markerPoint1 != null && ZRR.markerPoint2 != null) { // Créer la ZRR
		document.getElementById("ZRR_Description").innerHTML = 'Appuyez sur "Set" pour sélectionner un point de la carte :'
		if (ZRR.rectangleZRR != null) {
			mymap.removeLayer(ZRR.rectangleZRR);
		}

		let bounds = [ZRR.markerPoint1.getLatLng(), ZRR.markerPoint2.getLatLng()];

		// create an orange rectangle
		ZRR.rectangleZRR = L.rectangle(bounds, { color: "#ff7800", weight: 1 });
		ZRR.rectangleZRR.addTo(mymap);
	}
}

/**
 * Trigger when a corner of the ZRR is move
 * @param {marker} markerPoint
 */
function updateDragPointZRR(markerPoint) {
	if (markerPoint.dragging._enabled) {
		updatePointsZRR(markerPoint);
		updateZRR();
	}
};

/**
 * Create a corner for the ZRR
 * @param {marker} markerPoint
 * @param {string} msg
 * @returns {marker} markerPoint
 */
function createPointZRR(markerPoint, msg, _lat, _lng) {
	markerPoint = L.marker([_lat, _lng], { draggable: 'true' });
	markerPoint.addTo(mymap).bindPopup('<strong>' + msg + '</strong>.').openPopup();
	markerPoint.on('dragend', () => updateDragPointZRR(markerPoint));
	updatePointsZRR(markerPoint);
	return markerPoint;
}

/**
 * Donne l'autorisation de poser le point 1 de la ZRR
 */
function corner1() {
	ZRR.setMarker1 = true;
	ZRR.setMarker2 = false;
}

/**
 * Donne l'autorisation de poser le point 2 de la ZRR
 */
function corner2() {
	ZRR.setMarker2 = true;
	ZRR.setMarker1 = false;
}


function sendZrr() {
	if(ZRR.rectangleZRR != null) {
		sendZrrToServer(ZRR.rectangleZRR);
	}
}

function clearZrr() {
	if (ZRR.rectangleZRR != null) {
		mymap.removeLayer(ZRR.rectangleZRR);
		mymap.removeLayer(ZRR.markerPoint1);
		mymap.removeLayer(ZRR.markerPoint2);
		ZRR.rectangleZRR = null;
		ZRR.markerPoint1 = null;
		ZRR.markerPoint2 = null;
		document.getElementById("lat1").value = "";
		document.getElementById("lon1").value = "";
		document.getElementById("lat2").value = "";
		document.getElementById("lon2").value = "";
	}
}
