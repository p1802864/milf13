let ADVENTURER = {
    markerJoueurs: [],
}

function renderAdventurer() {
    let markerPoint = undefined;
    if (state.start) {
        if (state.ressources.users != undefined && state.ressources.users != []) {
            const listeUtilisateur = state.ressources.users.map(
                (u) => formatListAdventurer(u)
            );
            const html = `
            <table class="table table-dark">
            <thead>
              
                <th scope="col">ID</th>
                <th scope="col">TTL</th>
                <th scope="col">SCORE</th>
                <th scope="col">POSITION</th>
              
            </thead>
            <tbody>
              <tr>
            ${listeUtilisateur.join('')}
            </tr>
            </tbody>
          </table>
    `
            ADVENTURER.markerJoueurs.forEach((element) => {
                mymap.removeLayer(element);
            })
            ADVENTURER.markerJoueurs = [];
            state.ressources.users.forEach(element => {
                let iconPlayer = undefined;
                if (element.ttl == 0) {
                    iconPlayer = playerDeadIcon;
                } else if (element.ttl == -1) {
                    iconPlayer = playerInvisibleIcon;
                } else {
                    iconPlayer = playerIcon;
                }
                markerPoint = L.marker([element.position.lat, element.position.lng], { icon: iconPlayer });
                markerPoint.addTo(mymap).bindPopup('<strong>' + element.id + '</strong>.');
                ADVENTURER.markerJoueurs.push(markerPoint);
            })
            document.getElementById('listeDesJoueurs').innerHTML = html;
        }
    }
}

function formatListAdventurer(adventurer) {
    if (adventurer.ttl == 0) {
        return `<tr>
        <td>${adventurer.id}</td>
        <td>MORT</td>
        <td>${adventurer.score}</td>
        <td>lat: ${adventurer.position.lat} lng: ${adventurer.position.lng}</td>
        </tr>`
    }
    else {
        return `<tr>
        <td>${adventurer.id}</td>
        <td>${adventurer.ttl}</td>
        <td>${adventurer.score}</td>
        <td>lat: ${adventurer.position.lat} lng: ${adventurer.position.lng}</td>
        </tr>`
    }
}

function clearUser() {
    document.getElementById('listeDesJoueurs').innerHTML = '';
    ADVENTURER.markerJoueurs.forEach((element) => {
        mymap.removeLayer(element);
    })
    ADVENTURER.markerJoueurs = [];
}
