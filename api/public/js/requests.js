const state = {
    apiUrl: "https://192.168.75.17/game/admin/",
    header() {
        const headers = new Headers();
        headers.set('Accept', 'application/json');
        headers.set('Content-Type', 'application/json');
        return headers;
    },

    ressources: [],
    start: false
};


// //////////////////////////////////////////////////////////////////////////////
// OUTILS génériques
// //////////////////////////////////////////////////////////////////////////////

// un filtre simple pour récupérer les réponses HTTP qui correspondent à des
// erreurs client (4xx) ou serveur (5xx)
function filterHttpResponse(response) {
    if (response.status >= 400 && response.status < 600) {
        throw new Error(`${response.status}: ${response.statusText}`);
    }
    else {
        return response;
    }
}

function filterHttpResponseJson(response) {
    return response
        .json()
        .then((data) => {
            if (response.status >= 400 && response.status < 600) {
                throw new Error(`${data.name}: ${data.message}`);
            }
            return data;
        })
        .catch((err) => console.error(`Error on json: ${err}`));
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Requêtes API
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// POST
///////////////////////////////////////////////////////////////////////////////

function sendStartToServer() {
    let url = state.apiUrl + 'start';
    fetch(url,
        {
            method: 'POST',
            headers: state.header(),
        })
        .then(filterHttpResponse)
        .then(function () {
            state.start = true;
        })
        .then(sendZrrToServer)
        .then(sendTTLToServer)
        .then(function () {
            sendTreasuresBis();
        })
}

function sendTreasureToServer(tabTreasures) {
    const treasures = tabTreasures.map(({ id, positions, type, trouver }) => ({ id: id, positions: positions, type: type, trouver: trouver }));
    let url = state.apiUrl + 'treasure';
    fetch(url,
        {
            method: 'POST',
            headers: state.header(),
            body: JSON.stringify(treasures)
        })
        .then(filterHttpResponse)
}

function sendResetGame() {
    let url = state.apiUrl + 'reset';
    fetch(url,
        {
            method: 'POST',
            headers: state.header(),
        })
        .then(filterHttpResponse)
        .then(function () {
            state.start = false;
            clearZrr();
            clearTreasures();
            clearUser();
            renderAdventurer();
        })
}

///////////////////////////////////////////////////////////////////////////////
// PUT
///////////////////////////////////////////////////////////////////////////////

function sendZrrToServer() {
    let url = state.apiUrl + 'zrr';
    fetch(url,
        {
            method: 'PUT',
            headers: state.header(),
            body: JSON.stringify(ZRR.rectangleZRR._bounds)
        })
        .then(filterHttpResponse)
}

function sendTTLToServer() {
    let ttl = document.getElementById("ttl").value;
    if (ttl != undefined) {
        let url = state.apiUrl + 'ttl';
        fetch(url,
            {
                method: 'PUT',
                headers: state.header(),
                body: JSON.stringify({ ttl })
            })
            .then(filterHttpResponse)
    }
}


///////////////////////////////////////////////////////////////////////
//GET
///////////////////////////////////////////////////////////////////////

function getRessources() {
    let url = state.apiUrl + 'ressources';
    fetch(url,
        {
            method: 'GET',
            headers: state.header(),
        })
        .then(filterHttpResponseJson)
        .then((res) => {
            console.log(res);
            state.ressources = res;
            renderAdventurer();
        })
}

function getZrr() {
    let url = state.apiUrl + 'ressources/zrr';
    fetch(url,
        {
            method: 'GET',
            headers: state.header(),
        })
        .then(filterHttpResponseJson)
        .then((res) => {
            state.ressources.zrr = res;
            let zrr = state.ressources.zrr;
            if (zrr._southWest != undefined && zrr._northEast != undefined) {
                document.getElementById('setMarker1').style.border = '';
                ZRR.markerPoint1 = createPointZRR(ZRR.markerPoint1, "Premier Point", zrr._southWest.lat, zrr._southWest.lng);
                ZRR.markerPoint2 = createPointZRR(ZRR.markerPoint2, "Second Point", zrr._northEast.lat, zrr._northEast.lng);
                document.getElementById("lat1").value = zrr._southWest.lat;
                document.getElementById("lon1").value = zrr._southWest.lng;
                document.getElementById("lat2").value = zrr._northEast.lat;
                document.getElementById("lon2").value = zrr._northEast.lng;
                updateZRR();
            }
            else {
                clearZrr();
            }
        })
}

function getUsers() {
    let url = state.apiUrl + 'ressources/users';
    fetch(url,
        {
            method: 'GET',
            headers: state.header(),
        })
        .then(filterHttpResponseJson)
        .then((res) => {
            console.log(res);
            state.ressources.users = res;
            renderAdventurer();
        })
}

function getTreasures() {
    let url = state.apiUrl + 'ressources/treasures';
    fetch(url,
        {
            method: 'GET',
            headers: state.header(),
        })
        .then(filterHttpResponseJson)
        .then((res) => {
            console.log(res);
            createTreasureWithTreasures(res);
        })
}


///////////////////////////////////////////////////////////////////////
//Cycles
///////////////////////////////////////////////////////////////////////

setInterval(function () {
    getUsers();
    getTreasures();
    renderAdventurer();
}, 5000);

setInterval(function () {
    if (state.start) {
        if (state.ressources.users != [] && state.ressources.users != undefined) {
            state.ressources.users.forEach((element) => {
                if (element.ttl > 0) {
                    element.ttl = element.ttl - 1
                }
            });
            renderAdventurer();
        }
    }
}, 1000);

setTimeout(function () {
    getZrr();
    getTreasures();
    getUsers();
}, 100)
