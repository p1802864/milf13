///////////////////////////////////////////////////////////////////////
// VARIABLES GLOABALS
///////////////////////////////////////////////////////////////////////

// Treasures
const TREASURES = {
	nbTreasure: 0,
	fireTriggered: false, // La partie est lancé
	treasuresLocal: [], // Trésor locaux
	treasuresSend: [], // Trésor envoyé au serveur
	treasureIcon: this.L.icon({
		iconUrl: "assets/treasure.png",
		iconSize: [38, 38],
		//iconAnchor: [22, 94],
		//popupAnchor: [-3, -76],
	}),
	treasureOpenIcon: this.L.icon({
		iconUrl: "assets/treasureOpen.png",
		iconSize: [38, 38],
		//iconAnchor: [22, 94],
		//popupAnchor: [-3, -76],
	}),
};


///////////////////////////////////////////////////////////////////////
// TREASURES FUNCTIONS
///////////////////////////////////////////////////////////////////////

function placeTreasure() {
	if (!TREASURES.fireTriggered) {
		if (ZRR.rectangleZRR != null) {
			if (TREASURES.treasuresSend != []) {
				TREASURES.fireTriggered = true;
				document.getElementById('feu').style.border = '3px solid red'
			}
		}
		else {
			document.getElementById("ZRR_Description").innerHTML = 'Appuyez sur "Set" pour sélectionner un point de la carte : <splacepan style="color:red;">CREER UNE ZRR SI VOUS VOULEZ PLACEZ DES COFFRES</span>'
		}
	}
	else if (TREASURES.fireTriggered && TREASURES.treasuresSend.length == 0) {
		TREASURES.fireTriggered = false;
		document.getElementById('feu').style.border = '';
	}

}

/**
 * Call when a double click happen on a treasure to delete it
 * It is not delete if the treasure was already sent to the server
 * @param {*} markerPoint
 */
function deleteTreasure(markerPoint) {
	let newMarkerPoint;
	for (let i = 0; i < TREASURES.treasuresLocal.length; i++) {
		if (TREASURES.treasuresLocal[i].marker == markerPoint) {
			for (let j = i + 1; j < TREASURES.treasuresLocal.length; j++) {
				mymap.removeLayer(TREASURES.treasuresLocal[j].marker);
				newMarkerPoint = L.marker([TREASURES.treasuresLocal[j].positions.lat, TREASURES.treasuresLocal[j].positions.lng], { icon: TREASURES.treasureIcon });
				TREASURES.treasuresLocal[j].id = TREASURES.treasuresLocal[j].id - 1;
				newMarkerPoint.addTo(mymap).bindPopup('<strong>' + TREASURES.treasuresLocal[j].type + ' ' + TREASURES.treasuresLocal[j].id + '</strong>.').openPopup();
				newMarkerPoint.on('dblclick', () => deleteTreasure(newMarkerPoint));
				TREASURES.treasuresLocal[j].marker = newMarkerPoint;
			}
			mymap.removeLayer(markerPoint);
			TREASURES.treasuresLocal.splice(i, 1);
			TREASURES.nbTreasure--;
		}
	}
	if (TREASURES.treasuresLocal.length == 0 && TREASURES.treasuresSend == 0) {
		ZRR.markerPoint1.dragging.enable();
		ZRR.markerPoint2.dragging.enable();
	}
}

function sendTreasures() {
	if (TREASURES.treasuresLocal.length > 0) {
		if (!state.start) {
			sendStartToServer();
		}
		else {
			sendTreasuresBis();
		}
	}
}

function sendTreasuresBis() {
	if (state.start) {
		TREASURES.fireTriggered = true;
		document.getElementById('feu').style.border = '3px solid red'

		TREASURES.treasuresLocal.forEach(element => {
			element.marker.valueOf()._icon.style.border = "1px solid red";
			TREASURES.treasuresSend.push(element);
		});
		sendTreasureToServer(TREASURES.treasuresLocal);
		TREASURES.treasuresLocal = [];
	}
}

function createTreasure() {
	ZRR.markerPoint1.dragging.disable();
	ZRR.markerPoint2.dragging.disable();
	TREASURES.nbTreasure++;

	let typeTreasure = document.getElementById('treasure-selected').value

	let markerPoint = L.marker([lat, lng], { icon: TREASURES.treasureIcon });
	markerPoint.addTo(mymap).bindPopup('<strong>' + typeTreasure + ' ' + TREASURES.nbTreasure + '</strong>.').openPopup();
	markerPoint.on('dblclick', () => deleteTreasure(markerPoint));

	return { id: TREASURES.nbTreasure, positions: markerPoint._latlng, type: typeTreasure, trouver: false, marker: markerPoint };
}

function createTreasureWithTreasures(treasures) {
	let markerPoint = undefined;
	let typeTreasure = undefined;
	let treasure = {};
	let treasuresTemp = [];
	if (treasures.length > 0) {
		if (TREASURES.treasuresSend != []) {
			TREASURES.treasuresSend.forEach((element) => {
				mymap.removeLayer(element.marker);
			})
		}
		TREASURES.nbTreasure = 0;
		placeTreasure();
	}
	treasures.forEach(element => {
		state.start = true;
		TREASURES.treasuresSend = [];
		TREASURES.nbTreasure++;
		typeTreasure = element.type;
		if (!element.trouver) {
			markerPoint = L.marker([element.positions.lat, element.positions.lng], { icon: TREASURES.treasureIcon });
		}
		else {
			markerPoint = L.marker([element.positions.lat, element.positions.lng], { icon: TREASURES.treasureOpenIcon });
		}
		markerPoint.addTo(mymap).bindPopup('<strong>' + typeTreasure + ' ' + element.id + '</strong>.');
		markerPoint.on('dblclick', () => deleteTreasure(markerPoint));
		markerPoint.valueOf()._icon.style.border = "1px solid red";
		treasure = { id: element.id, positions: markerPoint._latlng, type: typeTreasure, trouver: false, marker: markerPoint };
		treasuresTemp.push(treasure);
	})
	TREASURES.treasuresSend = treasuresTemp;
}

function clearTreasures() {
	if (TREASURES.treasuresSend != []) {
		TREASURES.treasuresSend.forEach((element) => {
			mymap.removeLayer(element.marker);
		})
	}
	TREASURES.treasuresSend = [];
	if (TREASURES.treasuresLocal != []) {
		TREASURES.treasuresLocal.forEach((element) => {
			mymap.removeLayer(element.marker);
		})
	}
	TREASURES.treasuresLocal = [];
	TREASURES.nbTreasure = 0;
	placeTreasure();
}
