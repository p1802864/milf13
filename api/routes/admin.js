const express = require('express')
const axios = require('axios')
const fs = require('fs')
const bodyParser = require('body-parser')
const { response } = require('express')
const router = express.Router()
const game = require('../ressources/variables.js')

////////////////////////////////////////////////////////////////////////
// USE
////////////////////////////////////////////////////////////////////////

// middleware that is specific to this router
router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))

////////////////////////////////////////////////////////////////////////
// GET
////////////////////////////////////////////////////////////////////////
router.get('/user/:userId', (req, res) => { // Permet de suivre un joueur en particulier pour suivre sa position et son ttl
    const users = JSON.parse(fs.readFileSync('ressources/users.json', 'utf-8'));
    const body = users.find(e => e.id == req.params.userId);
    res.send(body);
})


// ROUTE RESSOURCES
router.get('/ressources', (req, res) => { // permet de suivre la partie
    // handle success
    const ZRRDATA = fs.readFileSync('ressources/ZRR.json', 'utf-8')
    const usersDATA = fs.readFileSync('ressources/users.json', 'utf-8')
    const treasuresDATA = fs.readFileSync('ressources/treasures.json', 'utf-8')
    const ZRR = JSON.parse(ZRRDATA)
    const users = JSON.parse(usersDATA)
    const treasures = JSON.parse(treasuresDATA)
    const body = {
        "ZRR": ZRR,
        "users": users,
        "treasures": treasures
    }
    res.contentType("application/json")
    res.send(body)
})

router.get('/ressources/zrr', (req, res) => { // permet de suivre la partie
    // handle success
    const ZRRDATA = fs.readFileSync('ressources/ZRR.json', 'utf-8')
    const ZRR = JSON.parse(ZRRDATA)
    const body = ZRR;
    res.contentType("application/json")
    res.send(body)
})

router.get('/ressources/users', (req, res) => { // permet de suivre la partie
    // handle success
    const usersDATA = fs.readFileSync('ressources/users.json', 'utf-8')
    const users = JSON.parse(usersDATA)
    const body = users;
    res.contentType("application/json")
    res.send(body)
})

router.get('/ressources/treasures', (req, res) => { // permet de suivre la partie
    // handle success
    const treasuresDATA = fs.readFileSync('ressources/treasures.json', 'utf-8');
    const treasures = JSON.parse(treasuresDATA);
    const body = treasures;
    res.contentType("application/json");
    res.send(body);
})


////////////////////////////////////////////////////////////////////////
// PUT
////////////////////////////////////////////////////////////////////////
// define the ZRR
router.put('/zrr', (req, res) => { // place les limites du zrr selon deux positions
    // TODO: vérifier si on a un tableau de deux coordonées (coordonées = tableau de deux nombre) et éventuellement vérifier le sens de ces coordonées.
    const zrr = req.body;
    if (Object.keys(zrr).length == 2 && zrr._southWest != undefined && zrr._northEast != undefined) {
        fs.writeFileSync("ressources/ZRR.json", JSON.stringify(zrr));
        res.sendStatus(204);
    }
    else {
        res.sendStatus(400).send("Le format de la ZRR envoyé n'est pas bon");
    }
})

// define the ttl of players
router.put('/ttl', (req, res) => { // défini le ttl initial de la partie
    const newTTL = req.body;
    game.state.initTTL = newTTL.ttl;
    res.sendStatus(204);
})

////////////////////////////////////////////////////////////////////////
// POST
////////////////////////////////////////////////////////////////////////
router.post('/start', (req, res) => { // démarre la partie
    if (game.state.start == true) { // si la partie a déjà commencé
        res.sendStatus(409);  // 409 - already exist
        return;
    }
    game.state.start = true;
    newGame();
    res.sendStatus(200);
})

router.post('/reset', (req,res) => {
    game.state.start = false;
    fs.writeFileSync("ressources/treasures.json", JSON.stringify([]));
    fs.writeFileSync("ressources/users.json", JSON.stringify([]));
    fs.writeFileSync("ressources/ZRR.json", JSON.stringify({}));
    res.sendStatus(200);
})

router.post('/treasure', (req, res) => { // place un trésore 
    // TODO: vérifier si l'objet envoyé est bien un trésor ! Pas utile dans l'immédiat
    const newTreasures = req.body;
    const oldTreasures = JSON.parse(fs.readFileSync("./ressources/treasures.json", 'utf-8'));
    newTreasures.forEach(treasure => {
        oldTreasures.push(treasure);
    });
    const treasures = oldTreasures;
    fs.writeFileSync("ressources/treasures.json", JSON.stringify(treasures));
    res.sendStatus(201); // 201 - created
})


////////////////////////////////////////////////////////////////////////
// OTHERS
////////////////////////////////////////////////////////////////////////
function newGame() {
    fs.writeFileSync("ressources/treasures.json", JSON.stringify([]));
    fs.writeFileSync("ressources/users.json", JSON.stringify([]));
}

function decreaseTTL() {
    if (game.state.start) {
        let users = JSON.parse(fs.readFileSync("./ressources/users.json", 'utf-8'));
        if (users != []) {
            users.forEach((element) => {
                if(element.ttl > 0) {
                    element.ttl = element.ttl - 1
                }
            });
            fs.writeFileSync("ressources/users.json", JSON.stringify(users)); // on sauvegarde
        }
    }
}

setInterval(decreaseTTL, 1000);

module.exports = router
