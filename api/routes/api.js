const express = require('express')
const axios = require('axios')
const fs = require('fs')
const bodyParser = require('body-parser')
const { response } = require('express')
const router = express.Router()
const https = require('https');
const game = require('../ressources/variables.js')

////////////////////////////////////////////////////////////////////////
// Utilitie(S)
////////////////////////////////////////////////////////////////////////

const authenticate = (req, res) => {
    const agent = new https.Agent({
        rejectUnauthorized: false
    });
    return axios.get('https://localhost:8443/cas/authenticate', {
        params: {
            'jwt': req.header('Authorization'),
            'origin': (req.header('Origin') == undefined ? "https://192.168.75.17" : req.header('Origin')),
        },
        httpsAgent: agent,
    });

}

const isImgUrl = (url) => {
    if (typeof url !== 'string') {
        return false;
    }
    return (url.match(/^http[^\?]*.(jpg|jpeg|gif|png|tiff|bmp)(\?(.*))?$/gmi) !== null);
}


////////////////////////////////////////////////////////////////////////
// USE
////////////////////////////////////////////////////////////////////////

// middleware that is specific to this router
router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))


////////////////////////////////////////////////////////////////////////
// GET
////////////////////////////////////////////////////////////////////////

// ROUTE RESSOURCES
router.get('/ressources', (req, res) => { // permet de suivre la partie
    // handle success
    authenticate(req, res)
        .then(function (response) {
            const ZRRDATA = fs.readFileSync('ressources/ZRR.json', 'utf-8')
            const usersDATA = fs.readFileSync('ressources/users.json', 'utf-8')
            const treasuresDATA = fs.readFileSync('ressources/treasures.json', 'utf-8')
            const ZRR = JSON.parse(ZRRDATA)
            const users = JSON.parse(usersDATA)
            const treasures = JSON.parse(treasuresDATA)
            const body = {
                "ZRR": ZRR,
                "users": users,
                "treasures": treasures
            }
            res.contentType("application/json")
            res.send(body)
        })
        .catch(function (error) {
            if (error.response) {
                console.log(error.message);
                res.sendStatus(error.response.status);
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
                res.sendStatus(500);
            }
        });
})


////////////////////////////////////////////////////////////////////////
// PUT
////////////////////////////////////////////////////////////////////////

/**
 * Update user's position
 */
router.put('/ressources/:ressourceId', (req, res) => {
    authenticate(req, res)
        .then(function (response) {
            if (response.headers.login == req.params.ressourceId) { // Si le login d'authentification est le même que la ressource que l'on souhaite modifier
                const newPos = req.body

                if (Object.keys(newPos).length == 2 && typeof (newPos.lat) == "number" && typeof (newPos.lng) == "number") {
                    const id = req.params.ressourceId
                    const users = JSON.parse(fs.readFileSync("ressources/users.json", "utf-8"))
                    let ok = false
                    const result = users.map(e => {
                        if (e.id == id) {
                            e.position = newPos
                            ok = true
                        }
                        return e;
                    })


                    if (ok) { // l'utilisateur est dans le jeu et est modifié
                        fs.writeFileSync("ressources/users.json", JSON.stringify(result)); // on l'enregistre
                        distancePlayerTreasure();
                        res.sendStatus(200); // 200 - ok 
                    }
                    else { // l'utilisateur envoie sa position pour la première fois
                        result.push(
                            {
                                id: id,
                                url: "https://tube.ac-lyon.fr/lazy-static/avatars/79e36851-030e-44c3-b848-873ac9f0aa42.png",
                                position: newPos,
                                role: "adventurer",
                                ttl: game.state.initTTL,
                                treasures: [],
                                distanceTreasure: -1,
                                score: 0,
                            }
                        );
                        fs.writeFileSync("ressources/users.json", JSON.stringify(result)); // on l'enregistre
                        distancePlayerTreasure();
                        res.sendStatus(200); // 200 - ok 
                    }
                }
                else {
                    res.sendStatus(400)// Body is not corectly defined
                }
            }
            else {
                res.sendStatus(403)// User can only modify himself
            }

        })
        .catch(function (error) {
            if (error.response) {
                res.sendStatus(error.response.status)// Problème d'authentification (often 401 - unauthorized)
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
                res.sendStatus(500)
            }
        });
})

// define the home page route
router.put('/ressources/:ressourceId/image', (req, res) => {
    authenticate(req, res)
        .then(function (response) {
            if (response.headers.login == req.params.ressourceId) { // Si le login d'authentification est le même que la ressource que l'on souhaite modifier
                const newImg = req.body
                console.log(newImg)

                if (Array.isArray(newImg) && isImgUrl(newImg[0])) {
                    const id = req.params.ressourceId
                    const users = JSON.parse(fs.readFileSync("ressources/users.json", "utf-8"))
                    let ok = false

                    const result = users.map(e => {
                        if (e.id == id) {
                            e.url = newImg[0]
                            ok = true
                        }
                        return e
                    })

                    if (ok) {
                        fs.writeFileSync("ressources/users.json", JSON.stringify(result))
                        res.sendStatus(204) // 204 - no content 
                    }
                    else {
                        res.sendStatus(404)// User have not been found 
                    }
                }
                else {
                    res.sendStatus(400)// Body is not corectly defined
                }
            }
            else {
                res.sendStatus(403)// User can only modify himself
            }

        })
        .catch(function (error) {
            if (error.response) {
                res.sendStatus(error.response.status)// Problème d'authentification (often 401 - unauthorized)
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
                res.sendStatus(500)
            }
        });
})

function degrees_to_radians(degrees) {
    let pi = Math.PI;
    return degrees * (pi / 180);
}

function distanceBewteenTwoPoint(lat1, lat2, lng1, lng2) {
    return (
        Math.acos(
            Math.sin(degrees_to_radians(lat1)) *
            Math.sin(degrees_to_radians(lat2)) +
            Math.cos(degrees_to_radians(lat1)) *
            Math.cos(degrees_to_radians(lat2)) *
            Math.cos(degrees_to_radians(lng2 - lng1))
        ) *
        6371 *
        1000
    );
}

function distancePlayerTreasure() {
    let users = JSON.parse(fs.readFileSync("ressources/users.json", "utf-8"))
    let treasuresDATA = JSON.parse(fs.readFileSync('ressources/treasures.json', 'utf-8'))
    users.forEach((user) => {
        let minDist = -1;
        let minTreasure = {};
        if(user.ttl != 0) {
            treasuresDATA.forEach((treasure) => {
                if (!treasure.trouver) {
                    let distance = distanceBewteenTwoPoint(
                        treasure.positions.lat,
                        user.position.lat,
                        treasure.positions.lng,
                        user.position.lng
                    );
                    if (minDist == -1 || minDist > distance) {
                        minDist = distance.toFixed(1);
                        minTreasure = treasure;
                    }
                }
            });
            user.distanceTreasure = minDist;
            if (minDist < 2.0 && minDist != -1 && minTreasure != {} && !treasuresDATA[minTreasure.id - 1].trouver) {
                treasuresDATA[minTreasure.id - 1].trouver = true;
                user.treasures.push(treasuresDATA[minTreasure.id - 1]);
    
                if (treasuresDATA[minTreasure.id - 1].type == "lune" && user.ttl > 0) {
                    user.ttl = user.ttl + 15; // survie un peu plus longtemps
                }
                else if (treasuresDATA[minTreasure.id - 1].type == "dissimulation" && user.ttl > 0) { 
                    user.ttl = -1; // Invincible
                }
    
                else if (treasuresDATA[minTreasure.id - 1].type == "Bêta-X") {
                    user.ttl = 0; // Mort
                }
    
                else if (treasuresDATA[minTreasure.id - 1].type == "SimpleTresor") {
                    user.score = user.score + 1;
                }
    
                else if (treasuresDATA[minTreasure.id - 1].type == "MoyenTresor") {
                    user.score = user.score + 10;
                }
    
                else if (treasuresDATA[minTreasure.id - 1].type == "GrosTresor") {
                    user.score = user.score + 50;
                }
            }
        }
    })
    fs.writeFileSync("ressources/users.json", JSON.stringify(users))
    fs.writeFileSync("ressources/treasures.json", JSON.stringify(treasuresDATA))
}


////////////////////////////////////////////////////////////////////////
// OTHER(S)
////////////////////////////////////////////////////////////////////////

module.exports = router
