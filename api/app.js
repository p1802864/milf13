const express = require('express')
const routeApi = require('./routes/api')
const routeAdmin = require('./routes/admin')
const routeStatic = require('./routes/static')
const app = express()
const port = 3000

///////////////////////////////////////////////////////////////////////
// GET
///////////////////////////////////////////////////////////////////////

app.get('/', (req, res) => {
  res.send('Hello World!')
})


///////////////////////////////////////////////////////////////////////
// LISTEN
///////////////////////////////////////////////////////////////////////

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


///////////////////////////////////////////////////////////////////////
// USE
///////////////////////////////////////////////////////////////////////

/** 3. DEPLOIEMENT */
app.use("/api", routeApi) // Pour la partie publique
app.use("/admin", routeAdmin) // Pour la partie confidentielle
app.use("/static", routeStatic) // Pour les contenus statiques (Pour faciliter les tests)
/** water by guy */

app.use((req, res, next) => {
  res.status(404).send("Sorry can't find that! (404 not found)")
  next()
})
