var state = { // état de la partie
    start : false, // booléen a true si la partie est démarrer
    initTTL : 60,  // ttl initial de la partie (60 seconde par défaut) a mettre a tous les joueurs quand la partie commence 
};

exports.state = state;