const state = {
    header() {
        const headers = new Headers();
        headers.set('Accept', 'application/json');
        headers.set('Content-Type', 'application/json');
        return headers;
    },

    ressources: [],
    start: false
};


// //////////////////////////////////////////////////////////////////////////////
// OUTILS génériques
// //////////////////////////////////////////////////////////////////////////////

// un filtre simple pour récupérer les réponses HTTP qui correspondent à des
// erreurs client (4xx) ou serveur (5xx)
function filterHttpResponse(response) {
    if (response.status >= 400 && response.status < 600) {
        throw new Error(`${response.status}: ${response.statusText}`);
    }
    else {
        return response;
    }
}

function filterHttpResponseJson(response) {
    return response
        .json()
        .then((data) => {
            if (response.status >= 400 && response.status < 600) {
                throw new Error(`${data.name}: ${data.message}`);
            }
            return data;
        })
        .catch((err) => console.error(`Error on json: ${err}`));
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Requêtes API
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// POST
///////////////////////////////////////////////////////////////////////////////

async function sendStartToServer() {
    let url = apiPath+'/start';
    fetch(url,
        {
            method: 'POST',
            headers: state.header(),
        })
        .then(filterHttpResponse)
        .then(() => {return new Promise((resolve) => {resolve();})})
}

function sendTreasureToServer(tabTreasures) {
    const treasures = tabTreasures.map(({ id, positions, type }) => ({ id: id, positions: positions, type: type }));
    let url = apiPath+'/treasure';
    fetch(url,
        {
            method: 'POST',
            headers: state.header(),
            body: JSON.stringify(treasures)
        })
        .then(filterHttpResponse)
}


///////////////////////////////////////////////////////////////////////////////
// PUT
///////////////////////////////////////////////////////////////////////////////

function sendZrrToServer(zrr) {
    let url = apiPath+'/zrr';
    fetch(url,
        {
            method: 'PUT',
            headers: state.header(),
            body: JSON.stringify(zrr._bounds)
        })
        .then(filterHttpResponse)
}

function sendTTLToServer(ttl) {
    let url = apiPath+'/ttl';
    fetch(url,
        {
            method: 'PUT',
            headers: state.header(),
            body: JSON.stringify({ ttl })
        })
        .then(filterHttpResponse)
}


///////////////////////////////////////////////////////////////////////
//GET
///////////////////////////////////////////////////////////////////////

function getRessources() {
    let url = apiPath+'/ressources';
    fetch(url,
        {
            method: 'GET',
            headers: state.header(),
        })
        .then(filterHttpResponseJson)
        .then((res) => {
            state.ressources = res;
            renderAdventurer();
        })
}


///////////////////////////////////////////////////////////////////////
//Cycles
///////////////////////////////////////////////////////////////////////

setInterval(getRessources, 10000)