function renderAdventurer() {
    const listeUtilisateur = state.ressources.users.map(
        (u) =>
            `<li id="${u.id}">
                <p><strong>Joueur:</strong> ${u.id} - <strong>TTL:</strong> ${u.ttl.ttl} - <strong>lat:</strong> ${u.position[0]} <strong>lng:</strong> ${u.position[1]} </p>
            </li>`
    );
    const html = `
        <ul>
            ${listeUtilisateur.join('')}
        </ul>
    `
    document.getElementById('listeDesJoueurs').innerHTML = html;
}