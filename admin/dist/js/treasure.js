///////////////////////////////////////////////////////////////////////
// VARIABLES GLOABALS
///////////////////////////////////////////////////////////////////////

// Treasures
const TREASURES = {
	nbTreasure: 0,
	fireTriggered: false, // La partie est lancé
	treasuresLocal: [], // Trésor locaux
	treasuresSend: [], // Trésor envoyé au serveur
};


///////////////////////////////////////////////////////////////////////
// TREASURES FUNCTIONS
///////////////////////////////////////////////////////////////////////

function placeTreasure() {
	if (!TREASURES.fireTriggered) {
		if (ZRR.rectangleZRR != null) {
			TREASURES.fireTriggered = true;
			document.getElementById('feu').style.border = '3px solid red'
		}
	}
	else if (TREASURES.fireTriggered && TREASURES.treasuresSend.length == 0) {
		TREASURES.fireTriggered = false;
		document.getElementById('feu').style.border = '';
	}

}

/**
 * Call when a double click happen on a treasure to delete it
 * It is not delete if the treasure was already sent to the server
 * @param {*} markerPoint
 */
function deleteTreasure(markerPoint) {
	for (let i = 0; i < TREASURES.treasuresLocal.length; i++) {
		if (TREASURES.treasuresLocal[i].positions == markerPoint._latlng) {
			mymap.removeLayer(markerPoint);
			TREASURES.treasuresLocal.splice(i, 1);
		}
	}
}

async function sendTreasures() {
	if (TREASURES.treasuresLocal.length > 0) {
		TREASURES.treasuresLocal.forEach(element => {
			element.marker.valueOf()._icon.style.border = "1px solid red";
			TREASURES.treasuresSend.push(element);
		});
		if (!state.start) {
			TREASURES.fireTriggered = true;
			state.start = true;
			document.getElementById('feu').style.border = '3px solid red'
			await sendStartToServer();
		}
		sendTreasureToServer(TREASURES.treasuresLocal);
		TREASURES.treasuresLocal = [];
	}
}

function createTreasure() {
	ZRR.markerPoint1.dragging.disable();
	ZRR.markerPoint2.dragging.disable();
	TREASURES.nbTreasure++;

	let typeTreasure = document.getElementById('treasure-selected').value

	let markerPoint = L.marker([lat, lng]);
	markerPoint.addTo(mymap).bindPopup('<strong>' + typeTreasure + ' ' + TREASURES.nbTreasure + '</strong>.').openPopup();
	markerPoint.on('dblclick', () => deleteTreasure(markerPoint));

	return { id: TREASURES.nbTreasure, positions: markerPoint._latlng, type: typeTreasure, marker: markerPoint };
}