///////////////////////////////////////////////////////////////////////
// VARIABLES GLOABALS
///////////////////////////////////////////////////////////////////////

// ZRR
const ZRR = {
	markerPoint1: null, // Premier point pour définir la zone de la ZRR
	markerPoint2: null, // Second point pour définir la zone de la ZRR
	setMarker1: false, // Permet de placer le point 1 de la ZRR sur la carte
	setMarker2: false, // Permet de placer le point 2 de la ZRR sur la carte
	rectangleZRR: null // La zone de la ZRR
};


///////////////////////////////////////////////////////////////////////
// ZRR Functions
///////////////////////////////////////////////////////////////////////

function setTTL() {
	let ttl = document.getElementById("ttl");
	if (ttl != undefined) {
		sendTTLToServer(ttl.value);
	}
}

/**
 * Update corners of the ZRR
 */
function updatePointsZRR(markerPoint) {
	if (markerPoint._popup._content == '<strong>Premier Point</strong>.') {
		document.getElementById("lat1").value = markerPoint.getLatLng().lat;
		document.getElementById("lon1").value = markerPoint.getLatLng().lng;
	}
	else if (markerPoint._popup._content == '<strong>Second Point</strong>.') {
		document.getElementById("lat2").value = markerPoint.getLatLng().lat;
		document.getElementById("lon2").value = markerPoint.getLatLng().lng;
	}
}

/**
 * Create the ZRR
 */
function updateZRR() {
	if (ZRR.markerPoint1 != null && ZRR.markerPoint2 != null) { // Créer la ZRR
		if (ZRR.rectangleZRR != null) {
			mymap.removeLayer(ZRR.rectangleZRR);
		}

		let bounds = [ZRR.markerPoint1.getLatLng(), ZRR.markerPoint2.getLatLng()];

		// create an orange rectangle
		ZRR.rectangleZRR = L.rectangle(bounds, { color: "#ff7800", weight: 1 });
		ZRR.rectangleZRR.addTo(mymap);
	}
}

/**
 * Trigger when a corner of the ZRR is move
 * @param {marker} markerPoint
 */
function updateDragPointZRR(markerPoint) {
	if (markerPoint.dragging._enabled) {
		updatePointsZRR(markerPoint);
		updateZRR();
	}
};

/**
 * Create a corner for the ZRR
 * @param {marker} markerPoint
 * @param {string} msg
 * @returns {marker} markerPoint
 */
function createPointZRR(markerPoint, msg) {
	if (markerPoint != null) {
		mymap.removeLayer(markerPoint);
	}

	markerPoint = L.marker([lat, lng], { draggable: 'true' });
	markerPoint.addTo(mymap).bindPopup('<strong>' + msg + '</strong>.').openPopup();
	markerPoint.on('dragend', () => updateDragPointZRR(markerPoint));
	updatePointsZRR(markerPoint);
	return markerPoint;
}

/**
 * Donne l'autorisation de poser le point 1 de la ZRR
 */
function corner1() {
	if (!ZRR.setMarker1) {
		document.getElementById('setMarker1').style.border = '3px solid red';
		document.getElementById('setMarker2').style.border = '';
		ZRR.setMarker1 = true;
		ZRR.setMarker2 = false;
	}
	else {
		document.getElementById('setMarker1').style.border = '';
		ZRR.setMarker1 = false;
	}
}

/**
 * Donne l'autorisation de poser le point 2 de la ZRR
 */
function corner2() {
	if (!ZRR.setMarker2) {
		document.getElementById('setMarker2').style.border = '3px solid red';
		document.getElementById('setMarker1').style.border = '';
		ZRR.setMarker2 = true;
		ZRR.setMarker1 = false;
	}
	else {
		document.getElementById('setMarker2').style.border = '';
		ZRR.setMarker2 = false;
	}
}


function sendZrr() {
	sendZrrToServer(ZRR.rectangleZRR);
}

