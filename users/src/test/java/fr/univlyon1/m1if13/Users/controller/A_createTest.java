package fr.univlyon1.m1if13.Users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class A_createTest {
    @Autowired
    private MockMvc mockMvc;

    private String origin = "http://localhost";
    
    @Test
    @Order(2)
    public void success() throws Exception {
        String dummy = "{\"login\":\"lorette\",\"password\":\"paquerette\"}";

        mockMvc.perform( post("/users")
            .header("Origin", origin)
            .contentType(MediaType.APPLICATION_JSON)
            .content(dummy))
            .andExpect(status().isCreated());
    }

    @Test
    @Order(1)
    public void badRequest() throws Exception {
        String dummy = "{\"yogourt\":\"lorette\",\"password\":\"paquerette\"}";
        mockMvc.perform( post("/users")
            .header("Origin", origin)
            .contentType(MediaType.APPLICATION_JSON)
            .content(dummy))
            .andExpect(status().isBadRequest());

    }

    @Test
    @Order(1)
    public void tooShortPassword() throws Exception {
        String dummy = "{\"login\":\"lorette\",\"password\":\"a\"}";
        mockMvc.perform( post("/users")
            .header("Origin", origin)
            .contentType(MediaType.APPLICATION_JSON)
            .content(dummy))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Order(3)
    public void alreadyExist() throws Exception {
        String dummy = "{\"login\":\"lorette\",\"password\":\"paquerette\"}";
        mockMvc.perform( post("/users")
            .header("Origin", origin)
            .contentType(MediaType.APPLICATION_JSON)
            .content(dummy))
            .andExpect(status().isConflict());
    }
}
