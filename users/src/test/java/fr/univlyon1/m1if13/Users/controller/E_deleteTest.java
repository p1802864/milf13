package fr.univlyon1.m1if13.Users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class E_deleteTest {
    @Autowired
    private MockMvc mockMvc;

    private String origin = "http://localhost";

    @Test
    @Order(1)
    public void deleteUser() throws Exception {
        mockMvc.perform( delete("/users/lorette")
            .header("Origin", origin))
            .andExpect(status().isNoContent());
    }

    @Test
    @Order(2)
    public void deleteUnexistingUser() throws Exception {
        mockMvc.perform( delete("/users/lorette")
            .header("Origin", origin))
            .andExpect(status().isNotFound());
    }
}
