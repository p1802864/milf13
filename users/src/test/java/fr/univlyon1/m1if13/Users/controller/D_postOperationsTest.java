package fr.univlyon1.m1if13.Users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class D_postOperationsTest {
    @Autowired
    private MockMvc mockMvc;

    private String origin = "http://localhost";
    private static String token;

    @Test
    public void loginWrongLogin() throws Exception {
        mockMvc.perform( post("/login")
            .param("login", "sophie")
            .param("password", "pissenlit")
            .header("Origin", origin))
            .andExpect(status().isNotFound())
            .andExpect(header().doesNotExist("Authorization"));
    }

    @Test
    public void loginWrongPassword() throws Exception {
        mockMvc.perform( post("/login")
            .param("login", "lorette")
            .param("password", "paquerette")
            .header("Origin", origin))
            .andExpect(status().isUnauthorized())
            .andExpect(header().doesNotExist("Authorization"));
    }

    @Test
    public void authenticateWrongToken() throws Exception {
        mockMvc.perform(get("/authenticate")
            .param("jwt", "je suis un token")
            .param("origin", origin))
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void authenticateNoToken() throws Exception {
        mockMvc.perform(get("/authenticate")
            .param("origin", origin))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Order(1)
    public void login() throws Exception {
        MvcResult result = mockMvc.perform( post("/login")
            .param("login", "lorette")
            .param("password", "pissenlit")
            .header("Origin", origin))
            .andExpect(status().isNoContent())
            .andExpect(header().exists("Authorization"))
            .andReturn();
        
        D_postOperationsTest.token = result.getResponse().getHeader("Authorization");
    }

    @Test
    @Order(2)
    public void authenticate() throws Exception {
        mockMvc.perform(get("/authenticate")
            .header("Origin", origin)
            .param("jwt", token)
            .param("origin", origin))
            .andExpect(status().isNoContent());
    }
}
