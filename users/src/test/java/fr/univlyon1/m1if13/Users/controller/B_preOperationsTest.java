package fr.univlyon1.m1if13.Users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class B_preOperationsTest {  
    @Autowired
    private MockMvc mockMvc;

    private static String token;

    private String login = "lorette";
    private String password = "paquerette";

    private String origin = "http://localhost";

    @Test
    @Order(1)
    public void login() throws Exception {
        MvcResult result = mockMvc.perform( post("/login")
            .param("login", login)
            .param("password", password)
            .header("Origin", origin))
            .andExpect(status().isNoContent())
            .andExpect(header().exists("Authorization"))
            .andReturn();
        
        B_preOperationsTest.token = result.getResponse().getHeader("Authorization");

    }

    @Test
    @Order(2)
    public void authenticate() throws Exception {
        mockMvc.perform(get("/authenticate")
            .header("Origin", origin)
            .param("jwt", token)
            .param("origin", origin))
            .andExpect(status().isNoContent());
    }

    @Test
    @Order(3)
    public void authenticateWrongOrigin() throws Exception {
        mockMvc.perform(get("/authenticate")
            .header("Origin", origin)
            .param("jwt", token)
            .param("origin", "http://192.168.75.17"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Order(4)
    public void logout() throws Exception {
        mockMvc.perform(post("/logout")
            .param("login", login)
            .header("Origin", origin))
            .andExpect(status().isNoContent());
    }
}
