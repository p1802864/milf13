package fr.univlyon1.m1if13.Users.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class C_getPutTest {
    @Autowired
    private MockMvc mockMvc;

    private String origin = "http://localhost";
    private String dummy = "{\"login\":\"lorette\",\"password\":\"pissenlit\"}";

    @Test
    public void getUserJSON() throws Exception {
        MvcResult result = mockMvc.perform(get("/users")
            .header("Origin", origin)
            .header("Accept","application/json"))
            .andExpect(status().isOk())
            .andReturn();
        
        String content = result.getResponse().getContentAsString();
        assert content.contains("lorette");
    }

    @Test
    public void getUserHTML() throws Exception {
        MvcResult result = mockMvc.perform(get("/users")
            .header("Origin", origin)
            .header("Accept","text/html"))
            .andExpect(status().isOk())
            .andReturn();
        
        String content = result.getResponse().getContentAsString();
        assert content.contains("lorette");
    }

    @Test 
    public void changePassword() throws Exception {
        mockMvc.perform(put("/users/lorette")
            .contentType(MediaType.APPLICATION_JSON)
            .content(dummy)
            .header("Origin",origin))
            .andExpect(status().isNoContent());
    }

}
