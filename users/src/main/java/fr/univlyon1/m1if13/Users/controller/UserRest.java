package fr.univlyon1.m1if13.Users.controller;


import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import fr.univlyon1.m1if13.Users.DAO.UserDao;
import fr.univlyon1.m1if13.Users.model.User;
import fr.univlyon1.m1if13.Users.model.UserForm;

@CrossOrigin(maxAge = 3600)
@RestController
public class UserRest {
    
    @Autowired
    UserDao uDao;


////////////////////////////////////////////////////////////////////////
//GET
////////////////////////////////////////////////////////////////////////

    /**
     * Procédure pour récupérer la liste des utilisateurs en format JSON ou XML
     * @return Une List<String> contenant la liste des logins de utilisateurs en format JSON ou un code approrié.
     */
    @Operation(summary = "Get the list of users",
        description = "Get the list of users that have an account on the website",
        responses = {
            @ApiResponse(responseCode = "200", description = "Succcessful operation",
                content = {
                    @Content(
                        mediaType = "application/json",
                        examples = {
                            @ExampleObject(
								value = "[\"Jonathan\",\"Guillaume\"]"
                            )
                        }
                    ),
                    @Content(
                        mediaType = "application/xml",
                        examples = {
                            @ExampleObject(
								value = "[\"Jonathan\",\"Guillaume\"]"
                            )
                        }
                    ),
                    @Content(
                        mediaType = "text/html",
                        examples = {
                            @ExampleObject(
								value = "[\"Jonathan\",\"Guillaume\"]"
                            )
                        }
                    )
                }
            )
        }
    )
    @CrossOrigin(origins = {"http://192.168.75.17","https://192.168.75.17","http://localhost","http://localhost:3376"})
    @GetMapping(value = "/users", produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public List<String> getJson(){
        List<String> users = new ArrayList<>(uDao.getAll());
        return users;
    }

    /**
     * Procédure pour récupérer la liste des utilisateurs en format HTML
     * @return Une List<String> contenant la liste des utilisateurs en format HTML ou un code approrié.
     */
    @CrossOrigin(origins = {"http://192.168.75.17","https://192.168.75.17","http://localhost","http://localhost:3376"})
    @GetMapping(value = "/users", produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView getHtml(ModelAndView model){
        model.addObject("users", uDao.getAll());
        model.setViewName("users");
        return model;
    }


////////////////////////////////////////////////////////////////////////
//POST
////////////////////////////////////////////////////////////////////////

    /**
     * Procédure pour créer un nouvel utilisateur le body doit contenir du x-www-form-urlencoded
     * @param user classe UserForm
     * @return Un code HTTP approprié
     **/
    @Operation(summary = "Create a new user",
    description = "Create a new user",
    responses = {
        @ApiResponse(responseCode = "201", description = "User created"),
        @ApiResponse(responseCode = "409", description = "User already existing")
    })
    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Void> postUrlEncoded(@Validated UserForm user) {
        return post(user);
    }

    /**
     * Procédure pour créer un nouvel utilisateur Le body doit contenir du JSON
     * @param user classe UserForm
     * @return Un code HTTP approprié
     **/
    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> postJson(@RequestBody @Validated UserForm user) {
        return post(user);
    }

    /**
     * Procédure pour créer un nouvel utilisateur
     * @param user classe UserForm
     * @return Un code HTTP approprié
     **/
    private ResponseEntity<Void> post(UserForm user) {
        Set<String> users = uDao.getAll();
        if(!(users.contains(user.login))){
            User t = new User(user.login,user.password);
            uDao.save(t);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        else{
            throw new ResponseStatusException(HttpStatus.CONFLICT,"User already exists");
        }
    }


////////////////////////////////////////////////////////////////////////
//PUT
////////////////////////////////////////////////////////////////////////

    /**
     * Procédure pour changer le mot de passe d'un utilisateur
     * Le body doit contenir du JSON
     * @param login est contenu dans le path de URL
     * @param user classe UserForm
     * @return Un code HTTP approprié
     **/
    @Operation(summary = "Update the password of the user",
        description = "Update the password of the user",
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Choose between JSON or x-www-form-urlencoded body",
                    content = {
                        @Content(
                            mediaType = "application/json"
                        ),
                        @Content(
                            mediaType = "application/x-www-form-urlencoded"
                        )
                    }
        ),
        responses = {
            @ApiResponse(responseCode = "204", description = "Succcessful operation"),
            @ApiResponse(responseCode = "404", description = "User not found (wrong login)"),
        }
    )
    @PutMapping(value = "/users/{login}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> putJson(@PathVariable("login") String login, @RequestBody UserForm user) {
        return put(login,user);
    }

    /**
     * Procédure pour changer le mot de passe d'un utilisateur
     *  Le body doit contenir du x-www-form-urlencoded
     * @param login est contenu dans le path de URL
     * @param user classe UserForm
     * @return Un code HTTP approprié
     **/
    @PutMapping(value = "/users/{login}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT) // IMPORTANT because a bug with Swagger add a default code (200 Accepted) in the response code
    public ResponseEntity<Void> putUrlEncoded(@PathVariable("login") String login, @Validated UserForm user) {
        return put(login,user);
    }

    /**
     * Procédure pour changer le mot de passe d'un utilisateur
     * @param login le login de l'utilisateur dont le mot de passe doit être changer
     * @param user classe UserForm
     * @return Un code HTTP approprié
     **/
    private ResponseEntity<Void> put(String login, UserForm user) {
        Optional<User> t = uDao.get(login);
        try{
            User u = t.get();
            String[] passwordLogin = {user.password,login};

            uDao.update(u, passwordLogin);
        } catch(NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        return ResponseEntity.noContent().build();
    }


////////////////////////////////////////////////////////////////////////
//DELETE
////////////////////////////////////////////////////////////////////////

    /**
     * Procédure pour supprimer un utilisateur
     * @param login est contenu dans le path de URL
     * @return Un code HTTP approprié
     **/
    @Operation(summary = "Delete a user",
    description = "Delete a user",
    responses = {
        @ApiResponse(responseCode = "204", description = "Succcessful operation"),
        @ApiResponse(responseCode = "404", description = "User not found (wrong login)"),
    })
    @DeleteMapping(value = "/users/{login}")
    public ResponseEntity<Void> delete(@PathVariable("login") String login){
        Set<String> users = uDao.getAll();
        if(users.contains(login)) {
            uDao.delete(new User(login,""));
            return ResponseEntity.noContent().build();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found cannot be deleted (Wrong login");
        }
    }
    
}
