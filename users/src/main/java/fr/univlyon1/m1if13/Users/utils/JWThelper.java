package fr.univlyon1.m1if13.Users.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Classe qui centralise les opérations de validation et de génération d'un token "métier", c'est-à-dire dédié à cette application.
 *
 * @author Lionel Médini le sang
 */
public class JWThelper {
    private static final String SECRET = "big mama";
    private static final String ISSUER = "User milf13";
    private static final long LIFETIME = 1800000; // Durée de vie d'un token : 30 minutes ; vous pouvez le modifier pour tester
    private static final Algorithm algorithm = Algorithm.HMAC256(SECRET);
    private static final JWTVerifier connectVerifier = JWT.require(algorithm).withClaim("connected", true).build(); // Reusable verifier instance

    /**
     * Vérifie l'authentification d'un utilisateur grâce à un token JWT
     *
     * @param token le token à vérifier
     * @param origin pour vérifier si l'origine de la requête est la même que celle du token
     * @return le string du login
     */
    public static String verifyToken(String token, @NotNull String origin) throws NullPointerException, JWTVerificationException {
        JWTVerifier authenticationVerifier = JWT.require(algorithm)
                .withIssuer(ISSUER)
                .withAudience(origin) // Non-reusable verifier instance
                .build();

        authenticationVerifier.verify(token); // Lève une NullPointerException si le token n'existe pas, et une JWTVerificationException s'il est invalide
        DecodedJWT jwt = JWT.decode(token); // Pourrait lever une JWTDecodeException mais comme le token est vérifié avant, cela ne devrait pas arriver
        return jwt.getClaim("sub").asString();
    }

    /**
     * Vérifie dans le token si un user est administrateur
     *
     * @param token le token à vérifier
     * @return un booléen indiquant si le token contient un booléen connected à true
     */
    public static boolean verifyConnect(String token) {
        try {
            connectVerifier.verify(token);
            return true;
        } catch (JWTVerificationException e) {
            return false;
        }
    }

    /**
     * Crée un token avec les caractéristiques de l'utilisateur
     *
     * @param subject le login de l'utilisateur
     * @param connected   si l'utilisateur est connecté ou non
     * @param origin     l'origine de la requete 
     * @return le token signé
     * @throws JWTCreationException si les paramètres ne permettent pas de créer un token
     */
    public static String generateToken(String subject, boolean connected, String origin) throws JWTCreationException {
        return JWT.create()
                .withIssuer(ISSUER)
                .withSubject(subject)
                .withAudience(origin)
                .withClaim("connected", connected)
                .withExpiresAt(new Date(new Date().getTime() + LIFETIME))
                .sign(algorithm);
    }

}