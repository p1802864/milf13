package fr.univlyon1.m1if13.Users.controller;

import java.util.NoSuchElementException;
import java.util.Optional;

import javax.naming.AuthenticationException;

import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import fr.univlyon1.m1if13.Users.DAO.UserDao;
import fr.univlyon1.m1if13.Users.model.User;
import fr.univlyon1.m1if13.Users.utils.JWThelper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@CrossOrigin(maxAge = 3600)
@Controller
public class UserOperations {

    // Create and configure beans
    // Retrieve configured instance
    @Autowired
    private UserDao uDao;

    /**
     * Procédure utilisée pour connecter un utilisateur
     * @param login Le login de l'utilisateur. L'utilisateur doit avoir été créé préalablement
     * @param password Le mot de passe du compte
     * @return Une ResponseEntity<Void> avec le JWT dans le header "Authorization" si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @PostMapping(value = "/login")
    @CrossOrigin(origins = {"http://192.168.75.17","https://192.168.75.17","http://localhost","http://localhost:3376"})
    @Operation(summary = "Connect a user",
    description = "Connect the user and set the JWT token in the header 'Authorization'",
    responses = {
        @ApiResponse(
            responseCode = "204",
            description = "User connected and return the JWT token in the header 'Authorization'",
            headers =
                @Header(
                    name = "Authorization",
                    description = "JWT Token",
                    schema = @Schema(implementation = String.class))
        ),
        @ApiResponse(responseCode = "404", description = "User not found (Wrong Login)"),
        @ApiResponse(responseCode = "401", description = "Wrong password"),
        @ApiResponse(responseCode = "500", description = "JWT token not created")
        })
    public ResponseEntity<Void> login(@RequestParam("login") String login, @RequestParam("password") String password, @RequestHeader("Origin") String origin) {
        Optional<User> t = uDao.get(login);
        HttpHeaders responseHeaders = new HttpHeaders();

        try{
            User user = t.get();                                                 //throw NoSuchElementException
            user.authenticate(password);                                         //throw AuthenticationException
    
            if(user.isConnected()){
                String token = "";
                
                token = JWThelper.generateToken(login,user.isConnected(),origin);//throw JWTCreationException 
                responseHeaders.set("Authorization", token);
                responseHeaders.set("Access-Control-Expose-Headers", "Authorization");
            }
        } catch(NoSuchElementException  e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", e);
        } catch(AuthenticationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wrong password", e);
        } catch(JWTCreationException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "JWT not created", e);
        }

        return new ResponseEntity<Void>(responseHeaders,HttpStatus.NO_CONTENT);
    }

    /**
     * Réalise la déconnexion.
     * @param login Le login de l'utilisateur. L'utilisateur doit avoir été créé préalablement
     * @return Une ResponseEntity avec le code de statut 204 si le logout s'est bien passé, sinon 404 dans le cas où le login n'existe pas
     */
    @Operation(summary = "Disconnect a user",
    description = "Disconnect the user",
    responses = {
        @ApiResponse(responseCode = "204", description = "User disconnected"),
        @ApiResponse(responseCode = "404", description = "User not found (Wrong login)"),
        @ApiResponse(responseCode = "500", description = "JWT not created")
        })
    @CrossOrigin(origins = {"http://192.168.75.17","https://192.168.75.17","http://localhost","http://localhost:3376"})
    @PostMapping(value = "/logout")
    public ResponseEntity<Void> logout(@RequestParam("login") String login, @RequestHeader("Origin") String origin) {
        Optional<User> t = uDao.get(login);
        try{
            User user = t.get();                                                //throw NoSuchElementException
    
            if(user.isConnected()){
                user.disconnect();
            }
        } catch(NoSuchElementException  e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", e);
        } catch(JWTCreationException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "JWT not created", e);
        }

        return ResponseEntity.noContent().build();
    }

    /**
     * Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.
     * @param jwt Le token JWT qui se trouve dans le header "Authorization" de la requête
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @CrossOrigin(origins = {"http://192.168.75.17","https://192.168.75.17","http://localhost","http://localhost:3376"})
    @GetMapping(value = "/authenticate")
    public ResponseEntity<Void> authenticate(@RequestParam("jwt") String jwt, @RequestParam("origin") String origin) {
        try{
            String subject = JWThelper.verifyToken(jwt,origin);
            return ResponseEntity.noContent().header("login", subject).build();
        }
        catch(NullPointerException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no jwt", e);
        }
        catch(JWTVerificationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "jwt unverified", e);
        } 
    }
}
