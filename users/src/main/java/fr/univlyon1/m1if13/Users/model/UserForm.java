package fr.univlyon1.m1if13.Users.model;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserForm {
    @NotBlank
    public String login;
    
    @NotEmpty
    @Size(min = 4,max = 32, message = "Password must be between 4 and 32 characters long")
    public String password;

    public UserForm(String login,String password){
        this.login = login;
        this.password = password;
    }

}
