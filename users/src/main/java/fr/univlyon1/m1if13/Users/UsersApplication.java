package fr.univlyon1.m1if13.Users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;


@OpenAPIDefinition(
            info = @Info(
                    title = "UserAPI",
                    version = "0.1",
                    description = "Set of functions that allows your applications to talk with our server",
                    contact = @Contact(name = "COGONI Guillaume and BREASSAT Jonathan")
            ),
			servers =
				@Server(
					description = "Server 1",
					url = "http://localhost:8080"
				)
			)
@SpringBootApplication
public class UsersApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(UsersApplication.class, args);
	}


	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(UsersApplication.class);
	}
}
