package fr.univlyon1.m1if13.Users.DAO;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Repository;

import fr.univlyon1.m1if13.Users.model.User;

@Repository
public class UserDao implements Dao<User>{

    private Map<String, User> users = new HashMap<>();

    private UserDao(){
        //on créé deux utilisateur pour les tests
        User tomi = new User("tomi","12345");
        User mandarine = new User("mandarine","12346");
        save(tomi);
        save(mandarine);
    }

    @Override
    public Optional<User> get(String id) {
        return Optional.ofNullable(users.get(id));
    }

    @Override
    public Set<String> getAll() {
        return users.keySet();
    }

    @Override
    public void save(User t) {
        users.put(t.getLogin(),t);
    }

    @Override
    public void update(User t, String[] params) {
        t.setPassword(Objects.requireNonNull(params[0],"password can not be null"));
        save(t);
    }

    @Override
    public void delete(User t) {
        users.remove(t.getLogin());
    }
    
}
