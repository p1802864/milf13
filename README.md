**ATTENTION :** bien qu'il soit interdit d'envoyer un commit après la date de rendu (pour le tp2), un commit urgence a du être envoyé pour permettre à l'évaluateur de pouvoir tester le serveur.<br/>
Ce commit ne change que le POM.xml et les propriétés de l'application.<br/>
Ce commit permet simplement de corriger un bug qui empêchait le déploiement correct sur Tomcat.<br/>
Le reste du code est inchangé, aucun ajout, aucune autre correction, aucune modification qui pourrait donnée un avantage quelconques.


# milf13

p1810070 COGONI Guillaume     
p1802864 BRESSAT Jonathan

Projet de M1if13 Web Avancé et Web Mobile

## TP1 & TP2 : Spring boot

- [l'api yaml](users-api.yaml)
- [l'api swagger](https://192.168.75.17:8443/cas/swagger-ui/index.html)
- [l'api d'authentification](https://192.168.75.17:8443/cas)


suite à une confusion la requête crud GET:/users/login} pour récupérer les informations d'un utilisateur est en fait devenue
GET:/users pour récupérer la liste des utilisateurs.

## TP3 & TP4 : api express & client administrateur

- [api publique](https://192.168.75.17/game/api)
- [api confidentiel](https://192.168.75.17/game/admin)
- [client administrateur](https://192.168.75.17/admin)

l'api publique demande à l'utilisateur de s'authentifier.
pour simplifier les choses l'api de l'administrateur ne demande aucune authentification ce qui permet de gérer uniquement des utilisateurs aventuriers.